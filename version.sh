#!/bin/bash

source version.conf

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --patch)
    patch=$[patch+1]
    shift
    ;;
    --minor)
    minor=$[minor+1]
    patch=0
    shift
    ;;
    --major)
    major=$[major+1]
    minor=0
    patch=0
    shift
    ;;
    --ext)
    OSTRING="$key"
    EXT="$2"
    shift
    shift
    ;;
    *)    # unknown option
    echo "usage: ./version.sh --patch|--minor|--major | --ext <string>"
    exit 1
    shift
    ;;
esac
done

version="v${major}.${minor}.${patch}"

#add extension?
if [ ! -z "$EXT" ] ; then
    version="${version}${EXT}"
fi

cat << EOF > version.conf
#!/bin/bash

major=$major

minor=$minor

patch=$patch

EXT=$EXT

version="${version}"

EOF

echo "Neue version:$version"
    sed -i "s/'version' =>.*$/'version' => \'$version\',/g" config/app.php
    sed -i "s/## Version.*$/## Version $version/g" README.md
exit 0
