@extends('layouts.app')

@section('content')
<div class="container" id="registration_container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Willkommen bei Cosum!</div>
                <div class="panel-body reddish_text">
                <p>
Cosum ist eine Leihplattform, auf der der Reichtum an Gegenständen geteilt werden kann. <br />
<br />
<strong>Keine unnötige Datenerhebung, </strong><strong>Keine Datenweitergabe, </strong><strong>Keine Werbung, </strong><strong>Kein Problem!</strong><br />
<br />
Unten registrieren und los geht's.<br />
Für Gemeinschaften gibt es im Kontaktbereich eine Möglichkeit die Kontaktliste mit den Menschen aufzufüllen, die du bereits kennst.
<br />
Bei Fragen und Anmerkungen einfach eine Mail an <a href="mailto:feedback@cosum.de" target="_top">feeback(at)cosum.de</a>. <br />
Auch zu technischen Fragen geben wir gerne Auskunft: <a href="mailto:support@cosum.de" target="_top">support(at)cosum.de</a>.
<br />
<br />
Wir freuen uns auf dich!<br /><br />
                </p>
                </div>
            </div>
        <h2>Registrierung</h2>
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Persönliche Angaben</div>
                <div class="panel-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail-Adresse</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <password-component ref="pw_comp" be_error="{{ $errors->first('password') }}" ></password-component> 
                         </div>
            </div>
        </div>

            <div class="col-md-6 col-md-offset-2">
                <button type="submit" class="btn btn-success front-btn">
                  abschicken
                </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section ('page_script_code')

@endsection

