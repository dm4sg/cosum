@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <h2>
                    Email-Verifikation
                </h2>
                </div>

                <div class="card-body">
                    <p>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Eine Verifikations-Email wurde soeben an die angegebene Adresse geschickt.
                        </div>
                    @endif

                    Vor dem Fortfahren bitte nach dem Verifikations-Link sehen.
                    Falls keine Email eingetroffen sein sollte, <a href="{{ route('verification.resend') }}">{{ __('hier klicken') }}</a>. Eine neue Email wird dann versendet.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
