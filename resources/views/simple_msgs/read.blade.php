@extends('layouts.app')

@section('content')
    <div class="row">
    <h1>Anfrage</h1>
        <div class="form-group top-buffer" hidden="hidden">
	</div>
		<div class="col-md-2 col-sm-2">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
			<img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
		</div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('text', 'Nachricht')}}
	    {!!$msg->text!!}
        </div>
       
    </div>
    
    @if ($msg->signal == null)
        <p>Noch unbeantwortet</p>
    @else
    <hr />
    <div class="row">
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer" >
            <h2>Antwort:</h2>
            @if ($msg->signal==1)
                <label for="signal_go">Ich sag' mal ja.</label>
                <input type="radio" name="signal" value="0" id="signal_go" checked="checked">
            @endif
            @if ($msg->signal==2)
                <label for="signal_no"> Geht leider nicht.</label>
                <input type="radio" name="signal" value="1" id="signal_no" checked="checked">
            @endif
	    {!!$msg->reply!!}
        </div>
    </div>
    @endif
    @endsection
