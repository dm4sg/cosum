@extends('layouts.app')

@section('content')
    <h1>Kontaktieren</h1>
    {!! Form::open(['action' => 'SimpleMsgController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::number('item_id', $gegenstand->id)}}
	    {{Form::number('from_user_id', auth()->user()->id ) }}
	    {{Form::number('to_user_id', $gegenstand->user_id ) }}
	</div>
		<div class="col-md-2 col-sm-2">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
			<img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
		</div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('text', 'Nachricht')}}
            {{Form::textarea('text', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Nachrichten Text'])}}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer" hidden="hidden">
            <label for="signal_go">Ich sag' mal ja.</label>
            <input type="radio" name="signal" value="0" id="signal_go">
            <br />
            <label for="signal_no"> Geht leider nicht.</label>
            <input type="radio" name="signal" value="1" id="signal_no">

            <?php //, 'readonly' => 'readonly' ?>
            {{Form::textarea('reply', '', ['id' => 'article-ckeditor2-dis', 'class' => 'form-control',  'placeholder' => 'Antwort Text'])}}
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
        </div>

    </div>
    {!! Form::close() !!}
    @endsection
