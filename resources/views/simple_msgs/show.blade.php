@extends('layouts.app')

@section('content')
    {!! Form::open(['action' => ['SimpleMsgController@reply', $msg->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
    <h1>Anfrage</h1>
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::text('item_id', $gegenstand->id)}}
	    {{Form::text('from_user_id', $gegenstand->user_id) }}
	    {{Form::text('to_user_id', auth()->user()->id ) }}
	</div>
		<div class="col-md-2 col-sm-2">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
			<img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
		</div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('text', 'Nachricht')}}
	    {!!$msg->text!!}
        </div>
       
    </div>
    <hr />
    <div class="row">
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer" >
            <h2>Antworten:</h2>
            <label for="signal_go">Ich sag' mal ja.</label>
            <input type="radio" name="signal" value="1" id="signal_go"
                @if ($msg->signal==0)
                    checked="checked"
                @endif
            >
            <br />
            <label for="signal_no"> Geht leider nicht.</label>
            <input type="radio" name="signal" value="2" id="signal_no"
                @if ($msg->signal==1)
                    checked="checked"
                @endif
            >

            <?php //, 'readonly' => 'readonly' ?>
            {{Form::textarea('reply', $msg->reply, ['id' => 'article-ckeditor', 'class' => 'form-control',  'placeholder' => 'Antwort Text'])}}
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
        </div>

    </div>
    {!! Form::close() !!}
    @endsection
