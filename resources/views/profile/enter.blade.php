@extends('layouts.app')

@section('content')
<div class="row">
            {!! Form::open(['action' => ['ProfileController@enter_community'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="col-md-8">
                <div class="panel-heading">Bilde Gemeinschaften mit realen Menschen!</div>
                <div class="panel-body reddish_text">
                <p>
Wenn du Teil einer Gemeinschaft bist oder in unserer Plattform eine neue Gemeinschaft einrichten lassen möchtest, dann schick uns eine Mail an <a href="mailto:gerard@cosum.de?Subject=Schlüssel" target="_top">gerard(at)cosum.de</a> oder <a href="mailto:nikolai@cosum.de?Subject=Schlüssel" target="_top">nikolai(at)cosum.de</a>
mit kurzer Beschreibung deiner Gemeinschaft. Du bekommst von uns in beiden Fällen einen <strong>Schlüssel</strong>, den du für die Verbindungen mit den Teilnehmern brauchst. Viel Freude!
                </p>
                </div>
            </div>
            <div class="col-md-8">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Gemeinschaft beitreten</div>

                <div class="panel-body">
                        <div class="form-group{{ $errors->has('keyname') ? ' has-error' : '' }}">
                            <label for="keyname" class="col-md-4 control-label">Schlüsselname</label>
                            <div class="col-md-6">
                                <input id="keyname" type="text" class="form-control" name="keyname" value="{{ old('keyname') }}" required>

                                @if ($errors->has('keyname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keyname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('keycode') ? ' has-error' : '' }}">
                            <label for="keycode" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="keycode" type="text" class="form-control" name="keycode" value="{{ old('keycode') }}" required>

                                @if ($errors->has('keycode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keycode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="auto_connect" class="col-md-4 control-label">Auto-Verbindung</label>
                            <div class="col-md-6">
                                {{Form::hidden('auto_connect',auth()->user()->auto_connect)}}
                                {{Form::checkbox('auto_connect')}}
                            <p>Ich möchte mit Gemeinschaftsmitgliedern automatisch verbunden werden. <br/> Der Benutzername wird bei einer Verbindung einmalig bekannt gegeben.</p>
                            </div>
                        </div>

                </div>
                </div>
                {{Form::submit('Beitreten', ['class'=>'btn btn-primary'])}}
        </div>
	</div>
                {!! Form::close() !!}
	</div>
@endsection
