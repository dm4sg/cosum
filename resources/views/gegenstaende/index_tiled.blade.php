@extends('layouts.app')

@section('content')
    @if (!Auth::guest())
        @component ('search.comp')
        @endcomponent
        <a href="/gegenstaende/create" class="btn btn-primary">Gegenstand anlegen</a>
    @else
        @component ('search.pub_comp')
        @endcomponent
    @endif
    <h1>
        @if ($dispersion == 'communal')
        Kommunale
        @elseif ($dispersion == 'public')
        Öffentliche
        @elseif ($dispersion == 'private')
        Private
        @endif
        &nbsp;Gegenstände
    </h1>

    @if (count($filters) > 0)
    Filter:
        @foreach ($filters as $filter)
            <label class="label label-info" for="">{{$filter}}</label> <br />
        @endforeach
    @endif
    <br />
    <div class="row">
    @if(count($gegenstaende) > 0)
        @foreach($gegenstaende as $gegenstand)
                    @if ( $loop->iteration % 4 == 0 )
                        <div class="row">
                    @endif
                    <div class="well col-md-3 col-sm-3">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                        @if ($gegenstand->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">Verliehen</span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$gegenstand->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                        @endif
                    </div>
                    @if ($loop->iteration % 4 == 0  ||  $loop->iteration == count($gegenstaende) )
                        </div > 
                    @endif
        @endforeach
        {{$gegenstaende->links()}}
    @else
        <p>Keine Gegenstände gefunden.</p>
    @endif
    </div>
@endsection
