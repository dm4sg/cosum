@extends('layouts.app')

@section('content')
    @if (!Auth::guest())
        <a href="/gegenstaende/create" class="btn btn-primary">Gegenstand anlegen</a>
    @endif
    <h1>
        Meine Gegenstände
    </h1>
    @if(count($gegenstaende) > 0)
        @foreach($gegenstaende as $gegenstand)
            <div class="well">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                        <a href="/gegenstaende/{{$gegenstand->id}}/edit" class="btn btn-sm btn-default">bearbeiten</a><br/><br/>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                    </div>
		    <div class="col-md-2 col-sm-2">
                        @if ($gegenstand->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">Verliehen</span>
                        An: <a class="label label-primary" href="/kontakte/{{$gegenstand->lent_to}}/edit">{{$gegenstand->lent_to_called}}</a>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$gegenstand->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                        @endif
                    </div>
		    <div class="col-md-3 col-sm-3">
                                        {!!Form::open(['action' => ['GegenstaendeController@destroy', $gegenstand->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('löschen', ['class' => 'btn btn-sm btn-danger'])}}
                                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        @endforeach
        {{$gegenstaende->links()}}
    @else
        <p>Keine Gegenstände gefunden.</p>
    @endif
@endsection
