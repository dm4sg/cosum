@extends('layouts.app')

  <script src="/js/jquery-1.12.4.js"></script>
  <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="/js/bootstrap-datepicker.min.js"></script>
@section('content')
    <h1>Gegenstand bearbeiten</h1>
    {!! Form::open(['action' => ['GegenstaendeController@update', $gegenstand->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="form-group col-md-6 cold-lg-6">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $gegenstand->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
                <div>
                @if ($gegenstand->gift)
                  <input type="checkbox" name="isgift" checked ><span> zu verschenken</span>
                @else
                  <input type="checkbox" name="isgift" ><span> zu verschenken</span>
                @endif
                </div>
            @if ($gegenstand->lent)
                <div class="" data-toggle="buttons">
                  <label for="islent" class="verleih_status btn btn-sm btn-default btn-danger active top-buffer"><input type="radio" name="islent" value="1" checked>verliehen</label>
                  <label for="islent" class="verleih_status btn btn-sm btn-default btn-success top-buffer"><input type="radio" name="islent" value="0">verfügbar</label>
                </div>
                <div class="input-group top-buffer" >
		   <span class="input-group-addon">
                        <label class="" >An:</label>
		   </span>
		   <input type="text" class="form-control" value="{{$gegenstand->lent_to_called}}" id="lent_to_called" placeholder="Empfänger" readonly="readonly" onclick="window.location='/kontakte/?lend={{$gegenstand->id}}';"/>
		   <span class="input-group-addon">
                        <span type='button' id="noone" class='' data-dismiss='alert' value="x" onclick="nullify('lent_to_contact_id');nullify('lent_to_called');">X</span>
		   </span>
                </div>
                <div class="form-group top-buffer" data-toggle="buttons" hidden="hidden">
                    {{Form::text('lent_to_contact_id', $gegenstand->lent_to_contact_id, [ 'id' => 'lent_to_contact_id', 'placeholder' => 'Empfänger']) }}
                </div>

		<div class='input-group date top-buffer' >
		    <span class="input-group-addon">
		    <label for="lent_to_date">Bis:</label>
		    </span>
                    <input id="lent_to_date" name="lent_to_date" type='text' class="form-control" data-provide="datepicker" value="{{$gegenstand->lent_to_date}}" />
		    <span class="input-group-addon">
		    <span class="glyphicon glyphicon-calendar"></span>
		    </span>
		</div>
            @else
                <div class="" data-toggle="buttons">
                  <label for="islent" class="verleih_status btn btn-sm btn-default btn-danger top-buffer"><input type="radio" name="islent" value="1">verliehen</label>
                  <label for="islent" class="verleih_status btn btn-sm btn-default btn-success active top-buffer"><input type="radio" name="islent" value="0" checked>verfügbar</label>
                </div>
            @endif
        </div>
    </div>
    <div class="row top-buffer">
        <div class="form-group  col-xs-8 col-sm-8 col-md-4 col-lg-4 top-buffer">
            {{Form::label('ortname', 'Leihort:')}}
            <?php // select code manually because of Form::select bug  ?>
            <select id="ortname" onchange="synch_id(this);">
                    <?php
                            foreach($ortnamen as $key => $name) {
                                    $selected='';
                                    if ($name == $ortname){ $selected='selected'; }
                    ?>
                                    <option value= <?php echo "\"$key\" " . "$selected >". $name ; ?></option>
                    <?php
                            }
                    ?>
            </select>
        </div>
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::label('sel_ort', 'Select OrtId')}}
	    {{Form::select('sel_ortid', $ortids , Input::old($gegenstand->ort_id), ['id' => 'sel_ortid']) }}
	</div>
	<div class="form-group top-buffer" hidden="hidden">
            {{Form::label('ortid', 'OrtId')}}
	    {{Form::text('ortid', $gegenstand->ort_id, ['id' => 'ortid']) }}
        </div>


	<div class="form-group  col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer" hidden="hidden">
            {{Form::label('dispersion', 'Verbreitungsgrad:')}}
	    {{Form::text('dispersion', $gegenstand->dispersion, ['id' => 'dispersion']) }}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12  ">
            {{Form::label('sel_dispersion', 'Sichtbarkeit:')}}
            <?php // select code manually because of Form::select bug  ?>
            <select name="sel_dispersion" id="sel_dispersion" onchange="synch_sel_field(this, 'dispersion');">
                <option value="{{$gegenstand->dispersion}}">@lang('dispersion.'.$gegenstand->dispersion) - gespeicherter Wert</option>
                <option value="public">öffentlich - Jeder kann diesen Gegenstand sehen.</option>
                <option value="communal">kommunal - Jeder Cosumer kann diesen Gegenstand sehen.</option>
                <option value="private">privat - Für andere nur durch gezielte Offenlegung sichtbar.</option>
                <!-- <option value="hidden">versteckt - Nur der Besitzer kann diesen Gegenstand sehen.</option> -->
            </select>
            <a class="btn btn-sm btn-primary" href="/gegenstaende/{{$gegenstand->id}}/offenlegen">Gegenstand offenlegen</a>
        </div>
        <div class="form-group  col-xs-8 col-sm-8 col-md-4 col-lg-4 top-buffer">
            {{Form::label('sel_category', 'Kategorie:')}}
            <select name="sel_category" id="sel_category" onchange="">
                <option value="{{$gegenstand->category}}" selected>{{$gegenstand->category}} - gespeicherter Wert</option>
                <option value="Baby & Kind">Baby & Kind
                </option>
                <option value="Garten">Garten
                </option>
                <option value="Werkzeug">Werkzeug
                </option>
                <option value="Sport & Spiele">Sport & Spiele
                </option>
                <option value="Haushalt">Haushalt
                </option>
                <option value="Outdoor">Outdoor
                </option>
                <option value="Medien">Medien
                </option>
                <option value="Elektronik">Elektronik
                </option>
                <option value="Sonstiges">Sonstiges
                </option>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('beschreibung', 'Beschreibung')}}
            {{Form::textarea('beschreibung', $gegenstand->beschreibung, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibung Text'])}}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
            {{Form::file('cover_image')}}
            *Maximale Dateigröße: 2MB.
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
        </div>

    </div>
    {!! Form::close() !!}
    @endsection

@section('page_script_code')
        (function() {
            //manually create language, for buggy include
            $.fn.datepicker.defaults.language = "de";
            $.fn.datepicker.noConflict;
            $.fn.datepicker.dates['de'] = {
                days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                daysShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                daysMin: ["So", "Mo", "di", "Mi", "do", "Fr", "Sa"],
                months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Julie", "August", "September", "Oktober", "November", "Dezember"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
                today: "Heute",
                clear: "Clear",
                format: "dd.mm.yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 0
            };

	    $('#lent_to_date').datepicker({language:'de'});
	})();

	function synch_id(that){
		var sel= document.getElementById('sel_ortid');
		sel.value=that.value;
		document.getElementById('ortid').value=sel.options[sel.selectedIndex].text;
        }
        function synch_sel_field(that, sel_id){
            var sel=document.getElementById(sel_id);
            sel.value=that.value;
            //var field=document.getElementById(field_id);
            //field.value=sel.options[sel.selectedIndex].text;
        }
        function nullify(field_id){
            var field = document.getElementById(field_id);
            field.value='';
        }

@endsection
