@extends('layouts.app')
@section('content')
<h1>Kontakt auswählen</h1>
     @if (count($contacts) > 0)
        @foreach ($contacts as $contact)
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <h4>
                            <a href="#" onclick="var his_id='{{$contact->id}}'; var sf=document.getElementById('selectionForm');sf.elements['new_contact_id'].value=his_id; sf.submit();" >
                                {{$contact->called}}
                            </a>
                        </h4>
                        <small>Angelegt am {{$contact->created_at}}</small>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        @if ($contact->status == 'made')
                            <h4>
                                <span class="glyphicon glyphicon-link"></span>
                                <small> <strong>@lang('contact.status.'.$contact->status)</strong> </small>
                            </h4>
                        @endif
                    </div>
                </div>
        @endforeach
        {!! Form::open(['id' => 'selectionForm', 'url' => $returnAddress]) !!}
            {{Form::hidden('new_contact_id', '', ['hidden' => 'hidden','class' => 'form-control', 'placeholder' => 'Kontakt id'])}}
        {!! Form::close() !!}
    @else
        <p>Keine Bekanntschaften</p>
    @endif
@endsection
