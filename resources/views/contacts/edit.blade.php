@extends('layouts.app')

@section('content')
	<h1>Kontakt:
		<i class="small">
			@lang('contact.status.'.$status)
		</i>
	</h1>

    {!! Form::open(['action' => ['ContactsController@update', $contact->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
		{{Form::label('called', 'Nenne ich:')}}
		{{Form::text('called', $contact->called, ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>

        @if ($status != 'made')
            @if ($contact->new_token)
                    <div class="form-group">
                        {{Form::label('new_token', 'Generierte Marke:')}}
                        {{Form::text('new_token', $contact->new_token, ['class' => 'form-control', 'placeholder' => 'Kontaktmarke'])}}
                    </div>
            @else
                    <div class="form-group">
                        {{Form::label('token', 'Meine Marke:')}}
                            <a href="/kontakte/{{$contact->id}}/edit?token=new" class="btn btn-xs btn-warning">generieren</a>
                        {{Form::text('token', $contact->token, ['class' => 'form-control', 'placeholder' => 'Kontaktmarke'])}}
                    </div>
            @endif


            {{-- generate qrcode as responder --}}
            @if ($contact->initiator_url)
                <h3>Responder-Code:</h3>
                {!! DNS2D::getBarcodeHTML(Request::root().$contact->initiator_url, "QRCODE",8,8, '#009999') !!}

            {{-- generate qrcode as initiator --}}
            @elseif ($contact->token)
                <h3>Initiator-Code:</h3>
                {!! DNS2D::getBarcodeHTML(Request::root()."/kontakte/create?corr_token=".$contact->token, "QRCODE",8,8, '#009999') !!}

            @endif

            @if ($corr_token)
            <div class="form-group">
                    {{Form::label('contact_token', 'Seine Kontaktmarke:')}}
                    {{Form::text('contact_token', $corr_token, ['class' => 'form-control', 'placeholder' => 'Kontaktmarke'])}}
            </div>
            @else
            <div class="form-group">
                    {{Form::label('contact_token', 'Seine Kontaktmarke:')}}
                    {{Form::text('contact_token', $contact->correspondent_token, ['class' => 'form-control', 'placeholder' => 'Kontaktmarke'])}}
            </div>
            @endif

        @endif

    {{Form::hidden('_method','PUT')}}
    {{Form::submit('speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    {!!Form::open(['action' => ['ContactsController@destroy', $contact->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Löschen', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}

@endsection
