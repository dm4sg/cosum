@extends('layouts.app')

@section('content')
    <a href="/kontakte/create" class="btn btn-primary">Kontakt anlegen</a>
    <h1>Liste der Kontakte</h1>

    @if (count($contacts) > 0)
        @foreach ($contacts as $contact)
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                    @if ($corr_token)
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit?corr_token={{$corr_token}}">{{$contact->called}}</a>
                        </h4>
                    @elseif ($obj_id && !$lend)
                        <h4>
                        @if ($contact->status == 'made')
                            <a href="/offenlegungen/create?obj_id={{$obj_id}}&rcpt_id={{$contact->id}}">{{$contact->called}} </a>
                        @else
                            <label>{{$contact->called}} </label>
                        @endif
                        </h4>
                    @elseif ($lend)
                        <h4>
                            <a href="/gegenstaende/{{$lend}}/edit?lent_to={{$contact->id}}">{{$contact->called}}</a>
                        </h4>

                    @else
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}</a>
                        </h4>
                    @endif
                        <small>Angelegt am {{$contact->created_at}}</small>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        @if ($contact->status == 'made')
                            <h4>
                                <span class="glyphicon glyphicon-link"></span>
                                <small> <strong>@lang('contact.status.'.$contact->status)</strong> </small>
                            </h4>
                        @endif
                    </div>
                </div>
        @endforeach
    @else
        <p>Keine Bekanntschaften</p>
    @endif
@endsection
