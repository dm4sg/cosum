@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/blueboard" class="btn btn-sm btn-default btn-info front-btn">Übersicht</a>
    <a href="/infotexts/create" class="btn btn-sm btn-default btn-success front-btn">Text anlegen</a>
    <h1>Infotexte</h1>
    @if (count($infotexts) > 0)
        @foreach ($infotexts as $infotext)
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-10 col-sm-0">
                        <h3><a href="/infotexts/{{$infotext->id}}">{{$infotext->title}}</a></h3>
                        <small>Verfasst am {{$infotext->created_at}} von {{$infotext->user->name}}</small>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        {!!Form::open(['action' => ['InfotextsController@destroy', $infotext->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Löschen', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </div>

                </div>
            </div>
        @endforeach
        {{$infotexts->links()}}
    @else
        <p>Kein Eintrag gefunden</p>
    @endif
@endsection
