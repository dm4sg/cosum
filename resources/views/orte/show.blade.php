@extends('layouts.app')

@section('content')
    <a href="{{ URL::previous() }}" class="btn btn-default">Zurück</a>
    <h1>{{$ort->name}}</h1>
    @if ($ort->plz)
        <div class='input-group top-buffer' >
		    <span class="">
		    <span class="glyphicon glyphicon-pushpin"></span>
		    </span>
                    <span>
                    <a class="input" href="https://nominatim.openstreetmap.org/search.php?q={{$ort->plz}}">Auf Karte anzeigen</a>
                    </span>
        </div>
    @endif
    <div class="top-buffer">
        {!!$ort->beschreibung!!}
    </div>
    <hr>
    <small>Verfasst am {{$ort->created_at}} </small>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $ort->user_id)
            <a href="/orte/{{$ort->id}}/edit" class="btn btn-default">bearbeiten</a>

            {!!Form::open(['action' => ['OrteController@destroy', $ort->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection
