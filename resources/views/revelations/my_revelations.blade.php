@extends('layouts.app')

@section('content')
    <h1>Meine Offenlegungen</h1>
    <h2>aus Gemeinschaften</h2>
    @foreach ($communities as $community)
        <h3 class="btn-default btn-warning">{{$community->keyname}}</h3>
        <div class="row">
        @foreach ($community->community_items()->get()->where('user_id',auth()->user()->id) as $gegenstand)
                    <div class="well col-md-3 col-sm-3">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                        @if ($gegenstand->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">Verliehen</span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$gegenstand->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                        @endif
                    </div>
        @endforeach

        </div>
    @endforeach
    <hr />
    <h2>Einzelne Offenlegungen</h2>
    @if(count($revelations) > 0)
        @foreach($revelations as $revelation)
            <div class="well">
                <div class="row">
                    <div class="col-md-3 col-sm-3 ">
                        <strong><a href="/gegenstaende/{{$revelation->obj_id}}">{{$revelation->obj->name}}</a></strong><br/>
                            @if ($revelation->obj->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$revelation->obj->cover_image}}">
                            @if ($revelation->obj->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                <b><label>{{$revelation->obj->category}}</label></b>
                            </span>
                            @endif
                    </div>
		    <div class="col-md-2 col-sm-2">
                        <strong>Von:<a href="/profil">mir</a></strong><br/>
                        <small>Geschrieben am {{$revelation->created_at}}</small>
                    </div>
		    <div class="col-md-2 col-sm-2">
                        <strong>An: <a href="/kontakte/{{$revelation->contact->id}}/edit">{{$revelation->contact->called}}</a></strong>
                    </div>
		    <div class="col-md-3 col-sm-3">
                        @if ($revelation->obj->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">Verliehen</span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$revelation->obj->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                        @endif
                    </div>
		    <div class="col-md-2 col-sm-2">
                        {!!Form::open(['action' => ['RevelationController@destroy', $revelation], 'method' => 'POST', 'class' => 'pull-right'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('zurücknehmen', ['class' => 'btn btn-sm btn-danger'])}}
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>Keine Offenlegungen gefunden.</p>
    @endif
@endsection
