@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/blueboard" class="btn btn-sm btn-default btn-info front-btn">Übersicht</a>
    <a href="/regkeys/create" class="btn btn-sm btn-default btn-success front-btn">Schlüssel anlegen</a>
    <h1>Schlüssel</h1>
    @if (count($regkeys) > 0)
        @foreach ($regkeys as $regkey)
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h3><a href="/regkeys/{{$regkey->id}}">{{$regkey->keyname}}</a></h3>
                        <h3>{{$regkey->keycode}}</h3>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <small>Verfasst am {{$regkey->created_at}} von {{$regkey->creator->name}}</small>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        {!!Form::open(['action' => ['RegkeysController@destroy', $regkey->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Löschen', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        {{Form::label('description', 'Beschreibung:')}}
                         {!!$regkey->description!!}
                    </div>
                </div>
            </div>
        @endforeach
        {{$regkeys->links()}}
    @else
        <p>Kein Eintrag gefunden</p>
    @endif
@endsection
