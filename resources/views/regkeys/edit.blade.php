@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/regkeys" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>Schlüssel bearbeiten</h1>
    {!! Form::open(['action' => ['RegkeysController@update', $regkey->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('keyname', 'Name/Gemeinschaft')}}
            {{Form::text('keyname', $regkey->keyname, ['class' => 'form-control', 'placeholder' => 'Titel'])}}
        </div>
        <div class="form-group">
            {{Form::label('keycode', 'Code')}}
            {{Form::text('keycode', $regkey->keycode, ['class' => 'form-control', 'placeholder' => 'Titel'])}}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', $regkey->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description Text'])}}
        </div>

        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-sm btn-info front-btn'])}}
    {!! Form::close() !!}
@endsection
