@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Kontaktkreis</div>

                <div class="panel-body">
                    <a href="kontaktkreise/create" class="btn btn-primary">Kontaktkreis anlegen</a>
                    <h3>Kontaktkreise</h3>
                    @if (count($contactcircles) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach ($contactcircles as $contactcircle)
                                <tr>
                                    <td><a class="tag label label-primary">{{$contactcircle->name}}</a></td>
                                    <td><a href="kontaktkreise/{{$contactcircle->id}}/edit" class="btn btn-default">bearbeiten</a></td>
                                    <td>
                                        {!!Form::open(['action' => ['ContactCirclesController@destroy', $contactcircle->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:40px;">
                                    @foreach ($contactcircle->contacts as $contact )
                                        <a class="tag label label-success" href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}
                                        <i class="remove glyphicon glyphicon-user glyphicon-white"></i>
                                    </a>
                                    @endforeach
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>Keine Kontaktkreis</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
