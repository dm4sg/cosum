@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/proposals" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>{{$proposal->title}}</h1>
    <br><br>
    <div class="well">
        {!!$proposal->body!!}
    </div>
    <hr>
    <small>Verfasst am {{$proposal->created_at}} von {{$proposal->user->name}}</small>
    <hr>
    <a href="/proposals/{{$proposal->id}}/edit" class="btn btn-sm btn-info front-btn">bearbeiten</a>
@endsection
