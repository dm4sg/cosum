@extends ('layouts.app')

@section ('content')
    <a href="/gesuche/create" class="btn btn-primary">Gesuch hinzufügen</a>
    <h1>Gesuche</h1>
    @if (count($itemrequests) > 0)
        @foreach ($itemrequests as $itemrequest)
            <div class="gesuchotron">
                <div class="row">
                    <div class="col-md-10 col-sm-0">
                        <h3><a href="/gesuche/{{$itemrequest->id}}">{{$itemrequest->title}}</a></h3>
                    </div>
                    @if (Auth::user()->id == $itemrequest->user_id)
                    <div class="col-md-2 col-sm-2">
                        {!!Form::open(['action' => ['ItemRequestsController@destroy', $itemrequest->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Löschen', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    </div>
                    @endif

                </div>
            </div>
        @endforeach
        {{$itemrequests->links()}}
    @else
        <p>Kein Eintrag gefunden</p>
    @endif
@endsection
