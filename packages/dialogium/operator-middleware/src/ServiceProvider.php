<?php

namespace Dialogium\OperatorMiddleware;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/operator-middleware.php';

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views/operator-middleware/','operator-middleware');
        $this->publishes([
            self::CONFIG_PATH => config_path('operator-middleware.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'operator-middleware'
        );

        $this->app->bind('operator-middleware', function () {
            return new OperatorMiddleware();
        });
    }
}
