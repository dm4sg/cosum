<?php

namespace Dialogium\OperatorMiddleware\Facades;

use Illuminate\Support\Facades\Facade;

class OperatorMiddleware extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'operator-middleware';
    }
}
