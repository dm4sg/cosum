# Operator Middleware

[![Build Status](https://travis-ci.org/dialogium/operator-middleware.svg?branch=master)](https://travis-ci.org/dialogium/operator-middleware)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/dialogium/operator-middleware/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/dialogium/operator-middleware/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/dialogium/operator-middleware/badge.svg?branch=master)](https://coveralls.io/github/dialogium/operator-middleware?branch=master)

[![Packagist](https://img.shields.io/packagist/v/dialogium/operator-middleware.svg)](https://packagist.org/packages/dialogium/operator-middleware)
[![Packagist](https://poser.pugx.org/dialogium/operator-middleware/d/total.svg)](https://packagist.org/packages/dialogium/operator-middleware)
[![Packagist](https://img.shields.io/packagist/l/dialogium/operator-middleware.svg)](https://packagist.org/packages/dialogium/operator-middleware)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require dialogium/operator-middleware
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Dialogium\OperatorMiddleware\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Dialogium\OperatorMiddleware\Facades\OperatorMiddleware::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Dialogium\OperatorMiddleware\ServiceProvider" --tag="config"
```

## Usage

CHANGE ME

## Security

If you discover any security related issues, please email michael@dialogium.de
instead of using the issue tracker.

## Credits

- [Michael Scheppat](https://github.com/dialogium/operator-middleware)
- [All contributors](https://github.com/dialogium/operator-middleware/graphs/contributors)

This package is bootstrapped with the help of
[melihovv/laravel-package-generator](https://github.com/melihovv/laravel-package-generator).
