<?php

namespace Dialogium\Blueboard\Http\Controllers;

use Illuminate\Http\Request;
use TOSClassAlias;
use RegkeyClassAlias;
use LendingprocessClassAlias;
use GegenstandClassAlias;
use DispersionClassAlias;
use App\User;
use CO2;

class BlueBoardController extends Controller
{
    protected $co2;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','operator-user']);
        $this->co2 = new CO2();
    }

    public function blueboard(){
        $latest_tos=TOSClassAlias::latest()->first();
        $latest_tos_id=is_null($latest_tos)?0:$latest_tos->id;
        $regkey_count=RegkeyClassAlias::count();

        return view('blueboard::blueboard')->with([
                        'latest_tos_id' => $latest_tos_id,
                        'regkey_count' => $regkey_count,
                    ]);
    }

    public function statistics(){
        $operator_count=User::all()->where('operator')->count();
        $user_count=User::all()->count();

        $gegenstaende_count=GegenstandClassAlias::all()->count();
        $gegenstaende_pub_count=GegenstandClassAlias::where('Dispersion', DispersionClassAlias::ISPUBLIC)->count();
        $gegenstaende_communal_count=GegenstandClassAlias::where('Dispersion', DispersionClassAlias::COMMUNAL)->count();
        $gegenstaende_private_count=GegenstandClassAlias::where('Dispersion', DispersionClassAlias::ISPRIVATE)->count();

        $open_lending_count = LendingprocessClassAlias::whereNull('finished_at')->count();
        $finished_lending_count = LendingprocessClassAlias::whereNotNull('finished_at')->count();
        $finished_lending_procs= LendingprocessClassAlias::all();//whereNotNull('finished_at');
        $this->co2->calc($finished_lending_procs);
        $lending_count = LendingprocessClassAlias::count();

        return view('blueboard::statistics')->with([
                        'operator_count' => $operator_count,
                        'user_count' => $user_count,
                        'gegenstaende_count' => $gegenstaende_count,
                        'gegenstaende_pub_count' => $gegenstaende_pub_count,
                        'gegenstaende_communal_count' => $gegenstaende_communal_count,
                        'gegenstaende_private_count' => $gegenstaende_private_count,
                        'open_lending_count' => $open_lending_count,
                        'finished_lending_count' => $finished_lending_count,
                        'lending_count' => $lending_count,
                        'co2' => $this->co2
                    ]);
    }

}
