@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
                <h1>Blueboard</h1>
                <div class="row alert alert-warning">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-warning front-btn" href="/blueboard/statistics">Statistiken</a>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-warning front-btn" href="/blueboard/user_management">Benutzer-Verwaltung</a>
                        <a class="btn btn-sm btn-warning front-btn" href="/blueboard/item_management">Gegenstands-Verwaltung</a>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-danger front-btn" href="/api/gegenstaende">API-Daten</a>
                    </div>
                </div>

               <div class="row jumbotron">
                    <h2>Vereinbarungen</h2>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/proposals">Liste</a>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/proposals/create">hinzufügen</a>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                       <p>
                            Aktuelle Version: {{ $latest_tos_id }}
                        </p> 
                    </div>
                </div>
                <div class="row jumbotron">
                    <h2>Schlüssel</h2>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/regkeys">Liste</a>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/regkeys/create">hinzufügen</a>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>Aktuelle Anzahl: {{ $regkey_count }}</p>
                    </div>
                </div>
                <div class="row jumbotron">
                    <h2>Info-Texte</h2>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/infotexts">Liste</a>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a class="btn btn-sm btn-primary front-btn" href="/infotexts/create">hinzufügen</a>
                    </div>
                </div>

@endsection
