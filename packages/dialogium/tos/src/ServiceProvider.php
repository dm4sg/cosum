<?php

namespace Dialogium\tos;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/tos.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('tos.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'tos'
        );

        $this->app->bind('tos', function () {
            return new tos();
        });
    }
}
