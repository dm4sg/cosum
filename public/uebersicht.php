<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="GtQvhtf46EjoJcCAZJQgzVM6AKjDA3tPwim0lX9E">
    <title>Cosum.de</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/body_pattern.css" rel="stylesheet" title="">
    <link href="/css/green_on_white.css" rel="stylesheet" title="green on white">
    <link href="/css/black_on_green.css" rel="alternate stylesheet" title="black on green">
    <link href="/css/post_styles.css" rel="stylesheet">
</head>
<body>
<?php 
    $cosum_instances = [
            0 => ['name' => 'Berlin', 'url' => 'https://berlin.cosum.de', 'descr' => 'Initiale Plattform von Cosum.de .','status' => 'success' ],
            1 => ['name' => 'Stadt Brandenburg','url' => 'https://brb.cosum.de', 'descr' => 'Wird eingerichtet' ,'status' => 'warning' ],
            /* 2 => ['name' => '...','url' => '#', 'descr' => 'Weitere Zweige in der Planung' ,'status' => 'info' ]*/
    ];
?>
<div id="app">
    <nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="brandname navbar-brand" href="#">
                <img src="/img/cosum_logo_and_claim_desat.png" width="200px"/>
            </a>
        </div>
    </div>
    </nav>
    <div class="container">
        <div class="col-md-12">
            <div class="jumbotron comic_item_back text-center">
                    <p>
                    <span ><img style="width:100%" src="/img/cosum_logo_and_claim_desat.png" /> </span>
                    </p>
                    <p style="color:white;font-weight:bold;">Die Plattform zum nachhaltigen Leihen und Schenken</p>
            <?php   foreach($cosum_instances as $inst){ ?>

                        <a class="btn btn-success front-btn" href="<?php echo $inst['url']; ?>">
                            <b>
                                <?php echo $inst['name']; ?>
                            </b>
                        </a>

            <?php 
                    }
            ?>


            </div>
        </div>

       <!-- <div class="col-md-12">
            <ul style="list-style:none;">
            </ul>
        </div> --!>
    </div>

        <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                <h1>Kontakt</h1>
                <p>
                <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:nikolai@cosum.de?Subject=[Cosum] Kontakt" target="_top">nikolai(at)cosum.de</a><br />
                <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:gerard@cosum.de?Subject=[Cosum] Kontakt" target="_top">gerard(at)cosum.de</a> <br />
                <a class="btn btn-xs btn-success front-btn top-buffer" href="http://cosum-blog.de">Zum Blog</a>
                </p>

                <a class="btn btn-lg btn-info front-btn top-buffer" href="htpps://berlin.cosum.de/impressum" target="_top">Impressum</a>
                <a class="btn btn-lg btn-info front-btn top-buffer" href="https://gitlab.com/dialogium/cosum" target="_top">OpenSource!</a>
                <div class="alert alert-info"><strong>Datenschutzerklärung:</strong> <br /><a href="/dokumente/datenschutz_v2.pdf">datenschutz_v2.pdf</a></div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <h2>Empfohlen von</h2>
                    <a href="https://www.bund-berlin.de/">
                        <img style="width:50%" src="/img/bund_weiss_transp.png">
                    </a>
                    <h2>Teil von</h2>
                    <img style="width:50%" src="/img/reuse-berlin.png">
                    <a href="http://zero-waste-berlin.de/" class="">
                        <img style="width:50%" src="/img/zero_waste.png">
                    </a>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class=".top-buffer"></div>
                    <h2>Gefördert durch</h2>
                    <a href="https://www.klimaschutz.de/" class="supporter">
                        <img style="width:100%;" src="/img/bmub_nki.jpg">
                    </a>
                    <a href="https://www.stiftung-naturschutz.de/startseite/" class="supporter">
                        <img style="width:50%" src="/img/stiftung_naturschutz.jpg">
                    </a>


                </div>
                <p>&nbsp;</p>
            </div>
        </div> <!-- container --!>
        </div> <!-- footer  --!>
</div> <!-- app  --!>
</body>
</html>
