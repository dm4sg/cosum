<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contact;
use App\Regkey;

class ContactCircle extends Model
{
    // Table Name
    protected $table = 'contactcircles';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function isCommunityCircle(){
        return Regkey::where('keyname', $this->name)->first() != null;
    }

    public function communityCircle(){
        return Regkey::where('keyname', $this->name)->first();
    }

    public function contacts(){
        return $this->belongsToMany('App\Contact', "contact_contactcircle","contactcircle_id", "contact_id")->withTimestamps();
    }
}
