<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Infotext;
use DB;

class InfotextsController extends Controller
{
    protected $view_source='';//blueboard::';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'operator-user']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infotexts = Infotext::orderBy('created_at','desc')->paginate(10);
        return view($this->view_source.'infotexts.index')->with('infotexts', $infotexts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_source.'infotexts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        // Create Infotext
        $infotext = new Infotext;
        $infotext->title = $request->input('title');
        $infotext->body = $request->input('body');
        $infotext->user_id = auth()->user()->id;
        $infotext->save();

        return redirect('/infotexts')->with('success', 'Vereinbarungstext hinzugefügt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $infotext = Infotext::find($id);
        return view($this->view_source.'infotexts.show')->with('infotext', $infotext);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $infotext = Infotext::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Seite');
        }

        return view($this->view_source.'infotexts.edit')->with('infotext', $infotext);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }

        // Create Infotext
        $infotext = Infotext::find($id);
        $infotext->title = $request->input('title');
        $infotext->body = $request->input('body');
        $infotext->user_id= auth()->user()->id;
        $infotext->save();

        return redirect('/infotexts')->with('success', 'Infotext Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $infotext = Infotext::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }
 
        $infotext->delete();
        return redirect('/infotexts')->with('success', 'Infotext Removed');
    }
}
