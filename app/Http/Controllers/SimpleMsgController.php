<?php

namespace App\Http\Controllers;

use App\Gegenstand;
use App\SimpleMsg;
use Illuminate\Http\Request;

class SimpleMsgController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }

    public function index(){
        $smsgs= SimpleMsg::where('to_user_id', auth()->user()->id)->orderBy('created_at','desc')->paginate(10);
        $sent_smsgs= SimpleMsg::where('from_user_id', auth()->user()->id)->orderBy('created_at','desc')->paginate(10);
        return view('simple_msgs.index')->with(['smsgs'=>$smsgs, 'sent_smsgs' => $sent_smsgs]);
    }

    public function create($gid, Request $request){
        $gegenstand = Gegenstand::find($gid);
        if (!$gegenstand){
            return redirect()->back()->with('error', 'Gegenstand nicht gefunden.' );
        }
            
        return view('simple_msgs.create')->with('gegenstand', $gegenstand);
    }

    public function show($id){
        $smsg=SimpleMsg::find($id);
        $gegenstand=Gegenstand::find($smsg->item_id);

        $view_mode=($smsg->signal == null)?'show':'read';
        //recipient
        if ($smsg->to_user_id == auth()->user()->id && $gegenstand && $gegenstand->user_id == auth()->user()->id )
                return view('simple_msgs.'.$view_mode)->with(['msg'=> $smsg, 'gegenstand' => $gegenstand]);
        
        //sender
        if ($smsg->from_user_id == auth()->user()->id && $gegenstand)
            return view('simple_msgs.read')->with(['msg'=> $smsg, 'gegenstand' => $gegenstand]);

        return redirect()->back()->with('error', 'Kein Zugriff.');
    }

    public function reply($id, Request $request){
        $this->validate($request, [
            'item_id' => 'required',
            'from_user_id' => 'required',
            'to_user_id' => 'required'
        ]);

        $msg=SimpleMsg::find($id);
        $item_id=$msg->item_id;;

        if ($msg->to_user_id !== auth()->user()->id )
            return redirect('/gegenstaende/'.$item_id)->with('error','Kein Zugriff.');

        $msg->reply= $request->input('reply'); 
        $msg->signal= $request->input('signal'); 
        $msg->save();
        return redirect('/gegenstaende/'.$item_id)->with('success','Anfrage beantwortet.');
    }

    public function store(Request $request){
        $this->validate($request, [
            'item_id' => 'required',
            'from_user_id' => 'required',
            'to_user_id' => 'required'
        ]);
        $item_id=$request->input('item_id');
        $to_user_id=$request->input('to_user_id'); 
        $gegenstand=Gegenstand::find($item_id);

        if ($gegenstand->user_id != $to_user_id){
            return redirect('/gegenstaende/'.$item_id)->with('error','Anfrage nicht möglich.');
        }

        $msg= new SimpleMsg;
        $msg->from_user_id = auth()->user()->id;
        $msg->to_user_id = $to_user_id; 
        $msg->item_id= $item_id;
        $msg->text= $request->input('text'); 

        $msg->save();

        return redirect('/gegenstaende/'.$item_id)->with('success','Anfrage entgegengenommen.');
    }
}
