<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Token;

class Contact extends Model
{
    // Table Name
    protected $table = 'contacts';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function owner(){
        return $this->belongsTo('App\User');
    }

    /* is_user
     * compares given user to containing user
     * on correspondent_id
    */
    public function is_user(User $user){
        $corr_id=$this->correspondent_id;
        $corr_user=User::find($corr_id);

        return ($corr_user && $user) && ($corr_user->id == $user->id);
    }

    public function contactcircles(){
        return belongsToMany('App\ContactCircle', "contact_id","contactcircle_id")->withTimestamps();
    }

    public function auto_connect(Contact $other_contact){
                $token1 = Token::Unique('contacts', 'token', 12);
                $token2 = Token::Unique('contacts', 'token', 12);
                $this->token=$token1;
                $other_contact->token=$token2;

                //key exchange
                $this->correspondent_token=$token2;
                $other_contact->correspondent_token=$token1;

                //Id exchange
                $this->correspondent_id=$other_contact->owner_id;
                $other_contact->correspondent_id=$this->owner_id;

                //save them
                $this->save();
                $other_contact->save();
    }

    public function status(){
	$res="initial";
	$corr_token=$this->correspondent_token;

	if (!$corr_token || $corr_token == ""){
	    return $res;
	}

        //other reachable?
	$his_contact=Contact::where('token',$corr_token)->first();
	if ($his_contact){
            $res="pending";

            //other regards me as correspondent
            $my_token=$this->token;
            if ($my_token && $his_contact->correspondent_token == $my_token )
                $res="made";

        }

	return $res;
    }

}
