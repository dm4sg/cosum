<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommunityToCccircle extends Migration
{
    /* correcting migration,
     field previously add to wong table 
    */

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add property to tell if this is a community circle
        Schema::table('contactcircles', function (Blueprint $table) {
            $table->boolean('community')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //remove community peroperty
        Schema::table('contactcircles', function (Blueprint $table) {
            $table->dropColumn('community');
        });
    }
}
