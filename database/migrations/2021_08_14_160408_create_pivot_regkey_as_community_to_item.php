<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class  CreatePivotRegkeyAsCommunityToItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regkey_gegenstand', function (Blueprint $table) {
            $table->increments('id');
             $table->unsignedInteger('regkey_id');
            $table->foreign('regkey_id')
                ->references('id')->on('regkeys')
                ->onDelete('cascade');
            $table->unsignedInteger('gegenstand_id');
            $table->foreign('gegenstand_id')
                ->references('id')->on('gegenstands')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regkey_gegenstand');
    }
}
